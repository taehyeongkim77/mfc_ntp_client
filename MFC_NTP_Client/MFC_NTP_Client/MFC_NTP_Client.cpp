﻿// MFC_NTP_Client.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include <iostream>

#include "Sntp.h"
#include "atlstr.h"

int TimeSync(TCHAR *pszServer = NULL)
{
	WSADATA wsaData;
	CSNTPClient sntp;
	NtpServerResponse response;
	CString strUrl;

	do
	{
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
			break;

		if (pszServer)
			strUrl = pszServer;
		else
			strUrl = _T("time.kriss.re.kr");	// 한국표준과학연구원 타임서버,  접속 원활치 않음

		if (!sntp.GetServerTime(strUrl, response)) // 서버 시간 가져오기
		{
			strUrl = _T("time2.kriss.re.kr"); // 한국표준과학연구원 타임서버2
			if (!sntp.GetServerTime(strUrl, response))
				break;
		}

		//시스템 시간을 서버 시간 차이를 더해서 동기화 시킨다.
		CNtpTime newTime(CNtpTime::GetCurrentTime() + response.m_LocalClockOffset);

		if (!sntp.SetClientTime(newTime))
			break;

	} while (0);

	WSACleanup();

	return 0;
}

int main()
{
    std::cout << "Hello World!\n"; 
	TimeSync("/* Server */");
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
